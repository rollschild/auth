const Authentication = require('./controllers/authentication');
const passportService = require('./services/passport');
const passport = require('passport');

// Middleware
const requireAuth = passport.authenticate('jwt', {session: false});
// do NOT create cookie session
// because we are using tokens
const requireSignin = passport.authenticate('local', {session: false});

module.exports = function(app) {
  /*
  app.get('/', function(req, res, next) {
    res.send(['basketball', 'baseball', 'ping pong']);
  });
  */

  // Protected route/resource
  app.get('/', requireAuth, function(req, res) {
    res.send({greeting: 'Hello, you!'});
  });

  app.post('/signup', Authentication.signup);
  app.post('/signin', requireSignin, Authentication.signin);
};
