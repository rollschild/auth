const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const Schema = mongoose.Schema;

// Define the model
const userSchema = new Schema({
  email: {type: String, unique: true, lowercase: true},
  password: String,
});

// On Save Hook, encrypt password
// Before saving a model (pre-save), run this function
userSchema.pre('save', function(next) {
  // get access to user model
  const user = this;

  bcrypt.genSalt(10, function(err, salt) {
    if (err) {
      return next(err);
    }

    // Hash password using salt
    bcrypt.hash(user.password, salt, null, function(err, hash) {
      if (err) {
        return next(err);
      }

      // Overwrite plain text password with encrypted
      user.password = hash;
      next();
    });
  });
});

// Object of currently defined methods on this schema.
userSchema.methods.comparePassword = function(candidatePassword, callback) {
  bcrypt.compare(candidatePassword, this.password, function(err, isMatched) {
    if (err) {
      return callback(err);
    }

    callback(null, isMatched);
  });
};

// Create model class
const ModelClass = mongoose.model('user', userSchema);

// Export the model
module.exports = ModelClass;
