const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const router = require('./router');
const mongoose = require('mongoose');
const cors = require('cors');

// Database setup
mongoose.connect('mongodb://auth:password@localhost:27017/auth');

const app = express();

// App setup
// Any incoming request will be passed into
// ...each middleware
app.use(morgan('combined'));
app.use(cors());

// parse incoming requests into json
app.use(bodyParser.json({type: '*/*'}));

router(app);

const port = process.env.PORT || 3090;

// create HTTP server
// forward request to the *app* express application
const server = http.createServer(app);
server.listen(port);
console.log('Server listening on: ', port);
