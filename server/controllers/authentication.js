const jwt = require('jwt-simple');
const User = require('../models/user');
const config = require('../config');

function genTokenFor(user) {
  const timestamp = new Date().getTime();
  // sub: subject
  // iat: issued at time
  return jwt.encode({sub: user.id, iat: timestamp}, config.secret);
}

exports.signup = function(req, res, next) {
  // res.send({success: 'true'});
  // Check if user with given email already exists
  const email = req.body.email;
  const password = req.body.password;

  if (!email || !password) {
    return res
      .status(422)
      .send({error: 'You must provide email and password!'});
  }

  User.findOne({email: email}, function(err, currUser) {
    if (err) {
      return next(err);
    }

    if (currUser) {
      // 422: unprocessable
      return res.status(422).send({error: 'Email is in use!'});
    }

    // Email does not exist
    // Create new user
    const user = new User({
      email: email,
      password: password,
    });

    user.save(function(err) {
      if (err) {
        return next(err);
      } else {
        res.json({token: genTokenFor(user)});
      }
    }); // save user to database
  });
};

exports.signin = function(req, res, next) {
  // User already has email and password auth'd
  // Give user a token
  res.send({token: genTokenFor(req.user)});
};
