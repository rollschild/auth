import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route} from 'react-router-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import reduxThunk from 'redux-thunk';

import App from './components/App';
import Welcome from './components/Welcome';
import Signup from './components/auth/Signup';
import reducers from './reducers';
import Features from './components/Features';
import Signout from './components/auth/Signout';
import Signin from './components/auth/Signin';

/*
 * Whenever App is rendered, the route is passed to
 * App as a prop with the name *children*
 */

const store = createStore(
  reducers,
  {
    auth: {authenticated: localStorage.getItem('token')},
  },
  applyMiddleware(reduxThunk),
);

// createStore(*default state object*)

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App>
        <Route path="/" exact component={Welcome} />
        <Route path="/signup" component={Signup} />
        <Route path="/features" component={Features} />
        <Route path="/signout" component={Signout} />
        <Route path="/signin" component={Signin} />
      </App>
    </BrowserRouter>
  </Provider>,
  document.querySelector('#root'),
);
