import axios from 'axios';
import {AUTH_USER, AUTH_ERROR_IN_USE} from './types';

// Action creator
export const signup = (formProps, callback) =>
  // normally, ALWAYS return an object
  // ...with a property *type*
  // ...and a *payload*
  // then sended to middleware -> reducers

  // thanks to redux-thunk:
  // redux-thunk allows us to have total control over
  // ...disptach
  async dispatch => {
    // be able to return/dispatch as many actions
    // ...we want from a single action creator
    // can do this multiple times
    // dispatch({type: AUTH_USER});
    try {
      const response = await axios.post(
        'http://localhost:3090/signup',
        formProps,
      );

      dispatch({type: AUTH_USER, payload: response.data.token});
      localStorage.setItem('token', response.data.token);
      callback();
    } catch (err) {
      dispatch({
        type: AUTH_ERROR_IN_USE,
        payload: 'Email already in use!',
      });
    }
  };

export const signin = (formProps, callback) => async dispatch => {
  try {
    const response = await axios.post(
      'http://localhost:3090/signin',
      formProps,
    );

    dispatch({type: AUTH_USER, payload: response.data.token});
    localStorage.setItem('token', response.data.token);
    callback();
  } catch (err) {
    dispatch({
      type: AUTH_ERROR_IN_USE,
      payload: 'Invalid login credentials!',
    });
  }
};

export const signout = () => {
  localStorage.removeItem('token');

  return {
    type: AUTH_USER,
    payload: '',
  };
};
